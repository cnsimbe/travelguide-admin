import { Component, ChangeDetectorRef, ElementRef, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { restApi } from '../../services/restApi';
import { baseUtils } from '../../utils/utils';
import { mapApi } from '../../services/mapApi';

@Component({
  selector:'map-bound',
  templateUrl: 'mapBounds.html'
})
export class mapBoundsPage {
  
  //public _location:Object;
  //public _locationPoint:Object[] = [];
  public _google;
  private map;
  private polygon;
  private drawingManager;
  private state:string =  null;
  public mapHeight:string;

  @Input('locationPoint') _locationPoint:Object[];
  @Input('location')_location:any;

  constructor(public navCtrl:NavController, private ref:ChangeDetectorRef, private utils:baseUtils, public mapApi: mapApi, private element:ElementRef ,private navParams:NavParams ,private restApi:restApi) {
    this.setHeight()
    window.addEventListener("resize",()=>{
        this.setHeight()
        this.ref.detectChanges()
    })
    window.addEventListener("orientationchange",()=>{
        this.setHeight()
        this.ref.detectChanges()
    })
  }

  ngOnInit()
  {
    this.mapApi.ready().then(_goo => this.initMap(_goo));
  }

  setHeight()
  {
    this.mapHeight = window.innerHeight*0.65 + 'px';
  }

  initMap(_google)
  {
    var me = this;
    this._google = _google;

    this._location["polygon"] = this._location["polygon"] || {"id":null};
    this._location["polygon"]["points"] = this._location["polygon"]["points"] || [];
    this._location["polygon"]["points"] = this._location["polygon"]["points"].map(o=>{
        o["lat"] = parseFloat(o["lat"]);
        o["lng"] = parseFloat(o["lng"]);
        return o;
      })
    this._location["lat"] = parseFloat(this._location["lat"]) || 49.2827;
    this._location["lng"] = parseFloat(this._location["lng"]) || -123.1207;
    this._location["zoom"] = this._location["zoom"] || 8;

    this.map = this.getMap()
    this.addMarkers()
    this.map.addListener('center_changed', function() {
      me.updateLocation()
    });

    this.map.addListener('zoom_changed', function() {
      me.updateLocation()
    });

    this.drawingManager = new _google.maps.drawing.DrawingManager({
          drawingControlOptions: {
            position: _google.maps.ControlPosition.TOP_CENTER,
          }
        });
    
    
    this._google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function(event) {
      if(event.type=='polygon')
        me.onPolygonComplete(event.overlay)
      else
        {
          event.overlay.setMap(null);
          event.overlay = null;
        }
    });


    if(this.hasPolygon())
    {
      this.polygon = this.getPolygon()
      this.setUpEdit()
    }
    else
    {
      this.setUpDraw()
    }  
  }



  getMap()
  {
    return new this._google.maps.Map(document.getElementById('maptag-location'), {
          center: {lat: this._location["lat"], lng: this._location["lng"]},
          zoom: this._location["zoom"]
        });
  }

  //construct polygon from location's polygon
  getPolygon()
  {
    return new this._google.maps.Polygon({
          paths: this._location["polygon"]["points"]
        })
  }

  setUpDraw()
  {
      this.state = 'draw';
      this.drawingManager.setOptions({
        drawingControl: true,
        drawingControlOptions: {
          position: this._google.maps.ControlPosition.TOP_LEFT,
          drawingModes: ['polygon']
        }
      });

      if(this.polygon)
      {
        this.polygon.setMap(null)
        this.polygon = null;
        this.updatePolygon()
      }

      this.drawingManager.setMap(this.map)
  }

  setUpEdit()
  {
      this.state = 'edit';
      this.drawingManager.setMap(null);
      this.polygon.setMap(this.map)
      this.polygon.setEditable(true)
      var me = this;
      this._google.maps.event.addListener(this.polygon.getPath(), 'set_at', function() {
        me.updatePolygon()
      });

      this._google.maps.event.addListener(this.polygon.getPath(), 'remove_at', function() {
        me.updatePolygon()
      });

      this._google.maps.event.addListener(this.polygon.getPath(), 'insert_at', function() {
         me.updatePolygon()
      });



  }

  addMarkers()
  {
    var me = this;
    this._locationPoint.forEach(o=>{
        new me._google.maps.Marker({
          position:{lat: parseFloat(o["lat"]), lng:parseFloat(o["lng"])},
          map: me.map,
          title: o["name"]
        });
    })
  }

  boundIsValid()
  {
    return true
  }

  eraseBounds()
  {
    
    this.setUpDraw()
  }

  onPolygonComplete(polygon)
  {
    this.polygon = polygon;
    this.updatePolygon()
    this.setUpEdit()

  }

  updateLocation()
  {
    this._location["zoom"] = this.map.getZoom();
    this._location["lat"] = this.map.getCenter().lat();
    this._location["lng"] = this.map.getCenter().lng();
  }

  updatePolygon()
  {
    if(this.polygon)
      this._location["polygon"]["points"]  = this.polygon.getPath().b.map(o => o.toJSON())
    else
      this._location["polygon"]["points"] = []

    this.updateLocation()
  }

  hasPolygon():boolean
  {
    return this._location["polygon"]["points"].length > 2;
  }

}
