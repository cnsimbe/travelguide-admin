import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Navbar, AlertController,LoadingController } from 'ionic-angular';
import { restApi } from '../../services/restApi';
import { baseUtils } from '../../utils/utils';
import { auth } from '../../services/auth';
@Component({
  templateUrl: 'locationPage.html'
})
export class locationPage {
  
  @ViewChild('topNav') topNav:Navbar;
  public location:any = null;
  public selectedUser:any = null;
  public locationPoint:any[] = [];
  public adminusers:any[] = null;
  public draftlocation:Object ={};
  public draftlocationPoint:Object[] = [];
  public originalPointIds:Number[] = [];
  public pointtypes = [];



  public editState:string = null;
  constructor(public navCtrl:NavController, public auth:auth ,private alertCtrl: AlertController, private LoadingController:LoadingController,   private utils:baseUtils, private navParams:NavParams , private restApi:restApi) {}

  ionViewWillEnter()
  {
      this.location = {};
      var me = this;
      this.topNav.backButtonClick = (o=>me.beforeBackButtonLeave(me))
      this.adminusers =this.navParams.data['adminusers']
      this.pointtypes = this.navParams.data["pointtypes"];
      this.location = this.navParams.data['location'] || {name:'New Location', id:null};
      this.location["polygon"] = this.location["polygon"] || {"id":null};
      this.location["polygon"]["points"] = this.location["polygon"]["points"] || [];

      this.location["polygon"]["points"] = this.location["polygon"]["points"].map(o=>{
        o["lat"] = parseFloat(o["lat"]);
        o["lng"] = parseFloat(o["lng"]);
        return o;
      })

      this.location["lat"] = parseFloat(this.location["lat"]) || 49.2827;
      this.location["lng"] = parseFloat(this.location["lng"]) || -123.1207;
      this.location["zoom"] = parseFloat(this.location["zoom"]) || 8;
      this.location["price"] = parseFloat(this.location["price"]) || 0;

      this.locationPoint = this.navParams.data['locationPoint'] || [];

      this.locationPoint = this.locationPoint.map(o=>{
        o["id"] = parseFloat(o["id"]);
        o["radius"] = parseFloat(o["radius"]) || 10;
        o["lat"] = parseFloat(o["lat"]);
        o["lng"] = parseFloat(o["lng"]);
        this.originalPointIds.push(o["id"])
        return o;
      })

      this.navCtrl.canGoBack = this.canGoBack;


      if(this.adminusers && this.location.adminuser)
        this.selectedUser = this.adminusers["find"](o=> this.location.adminuser.id == o.id)


  }

  

  beforeBackButtonLeave(context)
  {
    var me = context || this;
    me.alertCtrl.create({
        title:"Save Updates",
        buttons: [
          {
            text: 'Save',
            handler: () =>me.saveLocation()
          },
          {
            text: 'Dont Save',
            role: 'cancel',
            handler: () => me.navCtrl.pop()
          }
        ]
      }).present()
    
  }
  onSelectUser()
  {
    this.location.adminuser = this.selectedUser
  }
  canGoBack()
  {
    return this.editState==null;
  }

  editMapBounds()
  {

    this.draftlocation = this.utils.clone(this.location);
    this.draftlocationPoint = this.utils.clone(this.locationPoint);

    this.editState =  'bounds';



  }

  saveMapBounds()
  {
    this.location = this.draftlocation;
    this.locationPoint = this.draftlocationPoint;
    this.draftlocation = null;
    this.draftlocationPoint = null;
    this.editState =  null;
  }

  exitMapBounds()
  {

    this.editState =  null;
    this.draftlocation = null;
    this.draftlocationPoint = null;
  }

  editMapPoints()
  {
    
    this.draftlocation = this.utils.clone(this.location);
    this.draftlocationPoint = this.utils.clone(this.locationPoint);
    this.editState =  'points';
  }

  saveMapPoints()
  {
    this.location = this.draftlocation;
    this.locationPoint = this.draftlocationPoint;
    this.draftlocation = null;
    this.draftlocationPoint = null;
    this.editState =  null;
  }

  exitMapPoints()
  {
    this.editState =  null;
    this.draftlocation = null;
    this.draftlocationPoint = null;
  }

  warningRequired(title, message)
  {
    this.alertCtrl.create({title:title, subTitle:message}).present()
  }

   upoints()
    {
      return Promise.all(this.locationPoint.map(o => {
        return this.restApi.updateLocationPoint(o)
      }))
    }

  dpoints(toDelete:Number[])
  {
    return Promise.all(toDelete.map(o => {
      return this.restApi.deleteLocationPoint({id:o})
    }))
  }

  saveLocation()
  {
    let prm  = this._saveLocation();
    prm ? prm.then(o=>o==true?this.navCtrl.pop():''): ''
  }

  _saveLocation():Promise<any>
  {
    this.location["price"] = parseFloat(this.location["price"])
    if(!this.location["name"])
    {
      this.warningRequired("Error","Location name is required")
      return
    }

    if(!this.location["description"])
    {
      this.warningRequired("Error","Location description is required")
      return
    }

    if(!this.location["price"] && this.location["price"] !=0)
    {
      this.warningRequired("Error","Location price is required")
      return
    }

    if(this.location["polygon"]["points"].length <=2)
    {
      this.warningRequired("Error","Location must have bounds")
      return
    }


    var me = this;
    var loader = this.LoadingController.create({
      content: "Please wait..."
    });
    loader.present()
    let prm = this.restApi.updateLocation(this.location).then(z=> {
      this.location = z.data;
      this.locationPoint = this.locationPoint.map(o => {
        o["location"] = z.data;
        return o;
      })
      var toDelete = this.originalPointIds.filter(o => this.locationPoint.findIndex(point => point["id"]==o)<0);

      if(toDelete.length > 0)
        return me.dpoints(toDelete).then(f => {return me.upoints()})
      else
        return me.upoints()
    })

    return prm.then(o=>{
      loader.dismiss()
      return true
    }).catch(o=>{
      loader.dismiss()
      me.alertCtrl.create({subTitle:"There was a problem saving this location"}).present()
    })
   
  }


}
