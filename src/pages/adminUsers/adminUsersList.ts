import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { restApi } from '../../services/restApi';
import { auth } from  '../../services/auth';
import { userEditPage }  from './userEditPage';

@Component({
  templateUrl: 'adminUsersList.html'
})
export class adminUsersList {
  

  public appName:string = 'CIT EEZ';
  public locations:any[] = [];
  public getAdminUsers:any[] = [];
  public HomePage:any = HomePage;
  public userEditPage:any = userEditPage;
  constructor(public navCtrl:NavController, public loadingCtrl:LoadingController, public alertCtrl:AlertController, public auth:auth, private restApi:restApi) {
    this.appName = "CIT EEZ"
  }

  ionViewWillEnter(){
    let loader = this.loadingCtrl.create({
      content: "loading..."
    })
    loader.present()
  	this.restApi.getAdminUsers().then(data => {
      this.getAdminUsers = data
      loader.dismiss()
    }).catch(o=>{
      loader.dismiss()
      this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present()
    })
  }

  editUser(user)
  {
    this.navCtrl.push(this.userEditPage, {user:user})
  }



  updateUser(user)
  {
  	this.restApi.updateAdminUsers(user)
    .then(data => user=data.data)
    .catch(o=>this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present())
  }


}
