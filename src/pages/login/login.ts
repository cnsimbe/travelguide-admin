import { Component } from '@angular/core';
import { baseUtils } from '../../utils/utils';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { auth } from '../../services/auth';
import { forgetPasswordPage } from '../forgetPassword/forgetPassword';
import { HomePage } from '../home/home';

@Component({
 	templateUrl: 'login.html'
})


export class loginPage extends baseUtils {
 
  private isLoggedIn:boolean = false;
  constructor(private auth: auth, public alertCtrl: AlertController, public loadingCtrl:LoadingController ,public navCtrl:NavController) {
  	super()
  }

   loginParams:any = {}
  _forgetPasswordPage:any = forgetPasswordPage;
  loginSubmit(form){
  	if(form.valid==true)
  	{
      let loader = this.loadingCtrl.create({
          content: "loading..."
        })
      loader.present()
  		this.auth.loginUser(this.loginParams).then(data => {
        loader.dismiss()
        if(data.success==true)
          this.isLoggedIn = true;
        data.success!=true ? this.alertCtrl.create({subTitle:"Invalid login Credentials"}).present() : this.navCtrl.setRoot(HomePage) }
        )
  	}
  }
}
