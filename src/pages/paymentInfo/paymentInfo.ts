import { Component, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { restApi } from '../../services/restApi';
import { auth } from  '../../services/auth';

@Component({
  templateUrl: './paymentInfo.html'
})
export class paymentInfo {

  public appName:string = 'CIT EEZ';
  public locations:any[] = [];
  public adminusers:any[] = [];
  public HomePage:any = HomePage;
  public searchParams:any = {};
  public entries = [];
  @ViewChild('searchForm') searchForm:FormControl;
  constructor(public navCtrl:NavController, public loadingCtrl:LoadingController, public alertCtrl:AlertController, public auth:auth, private restApi:restApi) {
    this.appName = "CIT EEZ"
  }

  ionViewWillEnter(){
   


    let loader2 = this.loadingCtrl.create({
      content: "loading..."
    })
    loader2.present()

    this.restApi.getLocations(null).then(o=>{
      loader2.dismiss()
      this.locations = o;
    }).catch(o=>{
        loader2.dismiss()
        this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present()
      })

  }
  getAdminUsers()
  {
    if(this.searchParams["locations"])
      {
        let ar = [];
        return this.locations.filter(o=>this.searchParams["locations"].indexOf(o["id"])>=0).map(o=>o["adminuser"]).filter((a,b,c)=> ar.indexOf(a["id"])>=0 ? false : (ar.push(a["id"]) || true))
     }
    else
      return []
  }
  
  locationChange(idlists)
  {
    if(this.searchParams["users"])
    {
      this.searchParams["users"] = this.searchParams["users"].filter(o=>idlists.indexOf(o)>=0)
      console.log(this.searchParams["users"])
    }
  }

  formSubmit()
  {
    if(this.searchForm.valid==true)
      {
          let loader = this.loadingCtrl.create({
            content: "loading..."
          })
          loader.present()
          this.restApi.getAdminPayments(this.formatParams(this.searchParams)).then(o=>{
              loader.dismiss()
              this.entries = o
            }).catch(o=>{
              loader.dismiss()
              this.alertCtrl.create({subTitle:"There was a problem loading this data"}).present()
          })
      }
    else
      this.alertCtrl.create({subTitle:"Fill in the form correctly"}).present()
  }

  formatParams(p)
  {
    let params = {};
    for(var i in p)
      params[i] = p[i];
    
    params["startdate"] = new Date(params["startdate"]).getTime()
    params["enddate"] = new Date(params["enddate"]).getTime()
    console.log(params)
    return params
  }

  resetForm()
  {
    this.entries = []
  }

  allUsersSet(user)
  {
    console.log(user)
  }

}
