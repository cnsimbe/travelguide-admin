import { Component } from '@angular/core';
import { NavController,AlertController,LoadingController } from 'ionic-angular';
import { locationPage } from '../location/locationPage';
import { restApi } from '../../services/restApi';
import { auth } from  '../../services/auth';

@Component({
  templateUrl: 'home.html'
})
export class HomePage {
  

  public appName:string = 'CIT EEZ';
  public locations:any[] = [];
  public locationPage:any = locationPage;
  constructor(public navCtrl:NavController, public loadingCtrl:LoadingController, public alertCtrl:AlertController ,public auth:auth, private restApi:restApi) {
    this.appName = "CIT EEZ"
  }

  ionViewWillEnter(){
    let loader = this.loadingCtrl.create({
      content: "loading..."
    })
    loader.present()
  	this.restApi.getLocations(null).then(data => {
      this.locations = data;
      loader.dismiss()
    }).catch(o=>{
      loader.dismiss()
      this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present()
    })
  }

  editLocation(location)
  {
    let prms = [];
    prms.push(this.restApi.getPointTypes(null))
    if(location==null)
      prms.push(Promise.resolve(null))
    else
      prms.push(this.restApi.getLocationPoints({id:location.id}))

    if(this.auth.user.is_superuser==true)
      prms.push(this.restApi.getAdminUsers())

    let loader = this.loadingCtrl.create({
      content: "loading..."
    })
    loader.present()
    Promise.all(prms).then(values => {
      loader.dismiss()
      let pointtypes = values[0];
      let data = values[1];
      let adminusers = values.length > 2 ? values[2] : null;
      this.navCtrl.push(this.locationPage, {location:location, adminusers:adminusers, pointtypes:pointtypes ,locationPoint:data})
    }).catch(o=>{
      loader.dismiss()
      this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present()
    })
  }



  updateLocation(location)
  {
  	this.restApi.updateLocation(location).then(data => location=data.data).catch(o=>{
      this.alertCtrl.create({subTitle:"There was a problem updating this information"}).present()
    })
  }


}
