import { Component, ElementRef,ChangeDetectorRef, EventEmitter , Input,Output } from '@angular/core';
import {  AlertController } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';




class browserRecorder
{
  public context;
  constructor(context)
  {
    this.context = context
  }

  startRecording(event)
  {
    var me = this.context;
    event.stopPropagation()
    event.preventDefault()
    navigator["getUserMedia"] = (navigator["getUserMedia"] ||
                          navigator["mozGetUserMedia"] ||
                          navigator["msGetUserMedia"] ||
                          navigator["webkitGetUserMedia"]);


    if (navigator["getUserMedia"]) {
      var constraints = { audio: true };
      var chunks = [];
      var onSuccess = function(stream) {
        me._mediaRecorder = new window["MediaRecorder"](stream);

        me._mediaRecorder.onstop = function(e) {
          if(me.isRecording==false)
          {
            chunks = []
            return;
          }
          me.isRecording = false;
          var blob = new Blob(chunks, { 'type' : 'audio/ogg; codecs=opus' });
          chunks = [];
          me._file = new File([blob], "new recording.ogg", {type:blob.type});
          me._file["content_type"] = me._file["type"];
          me.alertCtl.create({subTitle:"Make sure the audio is supported. Supported formats are mp3, wav"}).present()
          me.reader.abort()
          me.reader.onload = function () {
          me._file["url"] = me.sanitizer.bypassSecurityTrustResourceUrl(me.reader.result);
          me.ngModelChange.emit(me._file);
          var element = me.getFileElement()
          element.value =  null;
          me.ref.detectChanges();
          setTimeout(()=>me.ref.detectChanges(),1)
          }
          me.reader.readAsDataURL(blob)
          
       }

        me._mediaRecorder.ondataavailable = function(e) {
          chunks.push(e.data);
        }
        me._mediaRecorder.start()
        me.isRecording = true;
        me.ref.detectChanges();
        setTimeout(()=>me.ref.detectChanges(),1)
      }

      var onError = function(err) {
        me.errorOpeningRecordingDevice();
        me.ref.detectChanges();
        setTimeout(()=>me.ref.detectChanges(),1)
      }

      navigator["getUserMedia"](constraints, onSuccess, onError);
    } else {
       me.audioRecordingNotSupported()
    }
  }

  saveRecording(event)
  {
    var me = this.context;
    event.stopPropagation()
    event.preventDefault()
    if(me._mediaRecorder && me._mediaRecorder.state=='recording')
    {
        me._mediaRecorder.stop();
        me._mediaRecorder = null;
    }
  }


  deleteRecording(event)
  {
    var me = this.context;
    event.stopPropagation()
    event.preventDefault()
    if(me._mediaRecorder && me._mediaRecorder.state=='recording')
    {
        me.isRecording = false
        me._mediaRecorder.stop();
        me._mediaRecorder = null;
    }
  }
}

@Component({
  selector:'ng-file',
  templateUrl: 'file-input.html',
})

export class fileInput {
  
  public _file:any = null;
  public _mediaRecorder:any;
  public isRecording:boolean = false;
  public maxFileNameLength:string;
  private reader = new FileReader();
  private recorder;
  public isMobile;
  @Input()set file(_f:Object){
    this._file = _f
  }
  @Input() name:string;
  @Output() ngModelChange = new EventEmitter();
  constructor(private element:ElementRef, private alertCtl:AlertController, private sanitizer:DomSanitizer ,private ref:ChangeDetectorRef) {

    this.setFileNameLength()
    window.addEventListener("resize",()=>{
        this.setFileNameLength()
        this.ref.detectChanges()
    })
  	window.addEventListener("orientationchange",()=>{
        this.setFileNameLength()
        this.ref.detectChanges()
    })
   
  }



  setFileNameLength()
  {
    this.maxFileNameLength = window.innerWidth*0.45 + 'px';
  }

  getFileElement()
  {
  	return this.element.nativeElement.querySelector("#uploadAudioInput")
  }

  ngAfterViewInit()
  {
    this.recorder = new browserRecorder(this);
    this.isMobile = false;
  	var me = this;
  	this.getFileElement().onchange = function(event)
  	{
      event.stopPropagation()
      event.preventDefault()
      event.stopImmediatePropagation()

  		var fileElement = me.getFileElement()
  		me._file = fileElement.files[0] as File || false;
      if(me._file)
      {
        me.reader.abort()
         me.reader.onload = function (e) {
           if(me._file)
             {
               me._file["url"] = me.sanitizer.bypassSecurityTrustResourceUrl(me.reader.result);
               me._file["content_type"] = me._file.type;
               me.alertCtl.create({subTitle:"Make sure the audio is supported. Supported formats are mp3, wav"}).present()
               me.ngModelChange.emit(me._file);
               me.ref.detectChanges();
             }
             else
               window.URL.revokeObjectURL(me._file);
             
         }
         me.reader.readAsDataURL(me._file)
      }
      else
        {
          me.ngModelChange.emit(me._file);
          me.ref.detectChanges();
        }
  	}
  }

  uploadClick(event)
  {
    event.stopPropagation()
    event.stopImmediatePropagation()
    this.getFileElement().click()
  }

  removeFile(event){
    this.reader.abort();
    this.recorder.deleteRecording(event)
  	event.stopPropagation()
  	event.preventDefault()
  	var element = this.getFileElement()
  	this._file = false;
  	element.value =  null;
  	this.ngModelChange.emit(this._file);


  }

  download()
  {

  }

  audioRecordingNotSupported()
  {
    this.alertCtl.create({title:"Audio Recording Not Supported"}).present()
  }

  errorOpeningRecordingDevice()
  {  
    this.alertCtl.create({title:"Error Opening Recording Device"}).present()
  }


  errorRecordingAudio()
  {  
    this.alertCtl.create({title:"Error Recording Aduio"}).present()
  }



}
