

export class baseUtils
{
	 exitApp(){}
	 objectToUrl(obj:any){
	 	try
	 	{
	 		var me = this;
	 		var str = Object.keys(obj).map(function(key) {
	 		if (typeof obj[key]== 'object')
	 			{
	 				return obj[key].map(n =>{
	 					var r = {}
	 					r[key] = n;
	 					return me.objectToUrl(r);
	 				}).join('&')
	 			}
	 		else
		    	return key + '=' + obj[key];
			}).join('&');
	 		return str;
	 	}
	 	catch(exp)
	 	{
	 		return ''
	 	}
	 	

		
	 }

	 clone(obj:Object):any{
	 	return JSON.parse(JSON.stringify(obj));
	 }
}