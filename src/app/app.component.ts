import { Component, ViewChild } from '@angular/core';
import { Platform,MenuController ,AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { NavController } from 'ionic-angular';
import { loginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { adminUsersList } from '../pages/adminUsers/adminUsersList';
import { paymentInfo } from '../pages/paymentInfo/paymentInfo';
import { auth } from '../services/auth';

@Component({
  templateUrl: 'app.html',
})
export class travelApp {
  rootPage:any;
  @ViewChild('mainNav') navCtrl: NavController;

  constructor(platform: Platform, private statusBar:StatusBar, public menuCtrl:MenuController, public auth:auth, private alertCtrl:AlertController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.auth.is_staff = true;
      statusBar.styleDefault();
    });
  }

  ngAfterViewInit()
  {
      this.menuCtrl.swipeEnable(false);
  		this.auth.isLoggedIn(null).then(data => {
  		data.success==false ? this.navCtrl.push(loginPage) : this.navCtrl.push(HomePage)
  	}).catch(o=>{
      this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present()
    })
  }

  openHome()
  {
    if(this.auth.user)
    this.navCtrl.setRoot(HomePage)
    else
      this.navCtrl.setRoot(loginPage)
  }

  logout()
  {
    this.auth.logoutUser().then(o=>{this.navCtrl.push(loginPage)})
  }

   login()
  {
    this.navCtrl.setRoot(loginPage)
  }

  openUsers()
  {
    if(this.auth.user && this.auth.user.is_superuser == true)
      this.navCtrl.setRoot(adminUsersList)
    else
      this.alertCtrl.create({title:"You are not authorized to view this page"}).present()
  }

  openPayments()
  {
    if(this.auth.user && this.auth.user.is_staff == true)
      this.navCtrl.setRoot(paymentInfo)
    else
      this.alertCtrl.create({title:"You are not authorized to view this page"}).present()
  }
}