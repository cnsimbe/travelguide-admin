import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { travelApp } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';

import { baseUtils } from '../utils/utils';
import { fileInput } from '../utils/file-input';
import { auth } from '../services/auth';
import { restApi } from '../services/restApi';
import { mapApi } from '../services/mapApi';

import { forgetPasswordPage } from '../pages/forgetPassword/forgetPassword';
import { locationPage } from '../pages/location/locationPage';
import { mapBoundsPage } from '../pages/location/mapBounds';
import { mapPointsPage } from '../pages/location/mapPoints';
import { loginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { userEditPage } from '../pages/adminUsers/userEditPage';
import { adminUsersList } from '../pages/adminUsers/adminUsersList';
import { paymentInfo } from '../pages/paymentInfo/paymentInfo';
import { StatusBar } from '@ionic-native/status-bar';

@NgModule({
  declarations: [
    travelApp,
    HomePage,
    fileInput,
    locationPage,
    forgetPasswordPage,
    mapBoundsPage,
    mapPointsPage,
    loginPage,
    adminUsersList,
    userEditPage,
    paymentInfo
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(travelApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    travelApp,
    HomePage,
    locationPage,
    fileInput,
    forgetPasswordPage,
    mapBoundsPage,
    mapPointsPage,
    loginPage,
    adminUsersList,
    userEditPage,
    paymentInfo
  ],
  providers: [restApi,mapApi,auth,baseUtils,StatusBar]
})
export class AppModule {}
